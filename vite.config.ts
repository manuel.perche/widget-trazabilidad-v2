import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import cssInjectedByJsPlugin from "vite-plugin-css-injected-by-js";

// https://vitejs.dev/config/
export default defineConfig({
  define: {
    "process.env": {
      NODE_ENV: "production",
    },
  },
  plugins: [react(), cssInjectedByJsPlugin()],
  build: {
    lib: {
      entry: "./src/Build.tsx",
      name: "trace",
      fileName: (format) => `trace.${format}.js`,
    },
    target: "esnext",
    outDir: 'public',
  },
  publicDir: 'assets'
});
