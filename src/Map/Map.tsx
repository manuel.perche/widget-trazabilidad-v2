import { useEffect, useMemo, useState } from "react";
import { GoogleMap, useLoadScript, Marker, InfoWindow } from "@react-google-maps/api";
import { useQuery } from "@apollo/client";
import { GET_PRODUCT_MAP } from "../graphql/getProductMap";

interface MapInterface {
  lat: number;
  lng: number;
  zoom: number;
  points: Points[];
}

interface Points {
  id: string;
  nombre: string;
  descripcion: string;
  location: MapLocation[];
}

interface MapLocation {
  lat: number;
  lng: number;
}

interface ProductById {
  map?: MapInterface[];
}

interface Data {
  product_by_id?: ProductById;
}

interface QueryResult {
  data?: Data;
}

interface MapConfig {
  points: Points[];
  config: {
    lat: number;
    lng: number;
    zoom: number;
  };
}

export interface IMapProps {
  productId: string;
}

export default function Mapa(props) {
  const params: any = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop: string) => searchParams.get(prop),
  });

  const [mapConfig, setMapConfig] = useState({
    lat: -31.904592035990216,
    lng: -63.83712558278997,
    zoom: 5,
  });

  const productId = props.productId ? props.productId : params.productId;

  const { data: { product_by_id: { map = [] } = {} } = {} }: QueryResult = useQuery(GET_PRODUCT_MAP, {
    variables: {
      id: productId,
    },
  });

  useEffect(() => {
    if (map.length > 0) {
      setMapConfig({
        lat: map[0].lat,
        lng: map[0].lng,
        zoom: map[0].zoom,
      });
    }
  }, [map]);

  // const defaultConfig = {
  //   lat: -31.904592035990216,
  //   lng: -63.83712558278997,
  //   zoom: 5,
  // };

  // const mapConfig =
  //   map.length > 0
  //     ? {
  //         lat: map[0].lat,
  //         lng: map[0].lng,
  //         zoom: map[0].zoom,
  //       }
  //     : defaultConfig;

  const points = map.length > 0 ? map[0].points : [];

  const { isLoaded } = useLoadScript({
    googleMapsApiKey: "AIzaSyCRDgSa3wKnMr1_QnQgRHVRDCOB-2uA3rc",
  });

  if (!isLoaded) return <div>Loading...</div>;

  return (
    <div className="map-body">
      <p onClick={() => console.log(mapConfig)}>TEST</p>
      <UcropitMap points={points} config={mapConfig} />
    </div>
  );
}

function UcropitMap({ points, config }: MapConfig) {
  const center = useMemo(() => ({ lat: config.lat, lng: config.lng }), []);
  const [selectedMarker, setSelectedMarker] = useState<Points>(null);

  return (
    <GoogleMap zoom={config.zoom} center={center} mapContainerClassName="mapaReact" mapContainerStyle={{ width: "100%", height: "100%" }}>
      {points.map((marker) => {
        return (
          <div key={marker.id}>
            <Marker
              key={marker.id}
              position={marker.location[0]}
              options={{}}
              onClick={() => {
                setSelectedMarker(marker);
              }}
            />
          </div>
        );
      })}
      {selectedMarker && (
        <InfoWindow
          position={selectedMarker.location[0]}
          options={{
            pixelOffset: new window.google.maps.Size(0, -40),
          }}
        >
          <div>
            <h6>{selectedMarker.nombre}</h6>
            <p>{selectedMarker.descripcion}</p>
            <button onClick={() => setSelectedMarker(null)}>close</button>
          </div>
        </InfoWindow>
      )}
    </GoogleMap>
  );
}
