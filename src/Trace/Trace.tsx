import { GET_STAGES } from "../graphql/getStages";
import { useQuery } from "@apollo/client";
import Timeline from "./Timeline";
import Card from "./Card";
import { GET_QR_BATCHES, GET_QR_BY_NAME } from "../graphql/getQrBatches";
import "./Trace.module.scss";
import { Button, Form } from "react-bootstrap";
import { useState } from "react";

export interface ITraceProps {
  productId: string;
  qrId?: string;
}

export const Trace = (props: ITraceProps) => {
  const [idForm, setIdForm] = useState("");
  const params: any = new Proxy(new URLSearchParams(window.location.search), {
    get: (searchParams, prop: string) => searchParams.get(prop),
  });

  const productId = props.productId ? props.productId : params.productId;
  const [qrId, setQrId] = useState(props.qrId ? props.qrId : params.qrId);

  const { data: { product_by_id: { stages = {} } = {} } = {} } = useQuery(GET_STAGES, {
    variables: {
      id: productId,
    },
  });

  const { data: { qr_by_id: { batches = {} } = {} } = {} } = useQuery(GET_QR_BATCHES, {
    variables: {
      id: qrId,
    },
  });

  const { data: { qr } = {} } = useQuery(GET_QR_BY_NAME, {
    variables: {
      name: qrId,
    },
  });

  const handleSearchQR = (e) => {
    e.preventDefault();
    setQrId(idForm);
  };

  if (!qrId) {
    return (
      <Form className="mb-3 mx-3" onSubmit={handleSearchQR}>
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>Ingrese ID de QR</Form.Label>
          <Form.Control type="text" placeholder="ID de QR" value={idForm} onChange={(e) => setIdForm(e.target.value)} />
        </Form.Group>
        <Button variant="primary" type="submit">
          Buscar
        </Button>
      </Form>
    );
  } else {
    return (
      <div className="trace-body">
        <Timeline>
          {stages.length > 0 &&
            stages.map((card: any, index: any) => {
              // const batch = qr && qr.length > 0 && qr[0].batches.length > 0 ? qr[0].batches.find((batch: any) => batch.stage_id.id === card.id) : {};

              const batch =
                batches && !(qr && qr.length > 0)
                  ? batches.length > 0
                    ? batches.find((batch: any) => batch.stage_id.id === card.id)
                    : {}
                  : qr && qr.length > 0 && qr[0].batches.length > 0
                  ? qr[0].batches.find((batch: any) => batch.stage_id.id === card.id)
                  : {};

              // const batch = batches.length > 0 ? batches.find((batch: any) => batch.stage_id.id === card.id) : {};

              const batchEvidences = batch?.evidences?.map((ev: any) => {
                return {
                  evidence_id: ev,
                };
              });

              const evidences = batch && batch?.evidences?.length > 0 ? [...card.evidences, ...batchEvidences] : card.evidences;

              const heading = batch && batch.quantity ? `${batch.quantity} ${batch.unit} de ${card.output}` : card.output;
              return (
                <Card
                  id={card.id}
                  key={index}
                  type={card.type}
                  title={card.title}
                  subtitle={batch ? batch.batch_number : card.subtitle}
                  icon={card.icon}
                  heading={heading}
                  location={card.location}
                  outputIcon={card.output_icon?.id}
                  by={card.by}
                  logoBy={card.logo_by?.id}
                  logo_right={card.logo_right}
                  inputs={card.inputs}
                  evidence={evidences}
                  batch={batch}
                />
              );
            })}
        </Timeline>
      </div>
    );
  }
};
