// var parse = require("html-react-parser");
import { SERVER_ASSETS } from "../utils/constants";
import { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import "./Proof.module.scss";

function Proof(props: any) {
  const { data, className } = props;

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  if (data.link) {
    return (
      <a
        href={data.link}
        target="_blank"
        className={className + " text-decoration-none text-reset d-flex align-items-center justify-content-between py-1 border-bottom"}
        rel="noreferrer"
      >
        {data.icon && <img src={`${SERVER_ASSETS}/${data.icon.id}`} className="m-2 trace-img-32" alt={data.title} width="32" height="32" />}

        <div className="mb-0 ms-1 w-100">
          <p className="card-title mb-0">{data.title}</p>
          <div className="card-subtitle mb-0 text-muted">
            <small>{data.subtitle}</small>
          </div>
        </div>

        <i className="bi bi-arrow-up-right-square ml-auto p-1"></i>
      </a>
    );
  }

  return (
    <>
      <li className={className + " d-flex align-items-center justify-content-between py-1 border-bottom"} role="button" onClick={handleShow}>
        {data.icon && <img src={`${SERVER_ASSETS}/${data.icon.id}`} className="m-2 trace-img-32" alt={data.title} width="32" height="32" />}

        <div className="mb-0 ms-1 w-100">
          <p className="card-title mb-0">{data.title}</p>
          <div className="card-subtitle mb-0 text-muted">
            <small>{data.subtitle}</small>
          </div>
        </div>

        <i className="bi bi-arrow-up-right-square ml-auto p-1"></i>
      </li>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <h5 className="modal-title fontOne textDark w-100">
            {data.title}
          </h5>
        </Modal.Header>
        <Modal.Body>
          {data.logo && <img src={`${SERVER_ASSETS}/${data.logo.id}`} className="mx-auto d-block w-50" alt={data.title} />}
          {/* <div className="mt-3">{data.description ? parse(data.description) : null}</div> */}
          {data.ots && (
            <div className="hint text-muted fw-light mt-4">
              Todos los documentos de respaldo se registran en blockchain para garantizar la fecha de su escritura. Para verificar el presente
              documento,
              <a href={`${SERVER_ASSETS}/${data.ots.id}`} download>
                {" "}
                descargue el archivo OTS
              </a>
              ,&nbsp;
              <a href={`${SERVER_ASSETS}/${data.evidence.id}`} download>
                descargue tambíen la evidencia
              </a>{" "}
              y dirijase a{" "}
              <a href="https://opentimestamps.org" target="_blank" rel="noreferrer">
                Open Timestamps
              </a>
            </div>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleClose}>
            Cerrar
          </Button>
          {data.evidence && (
            <a
              type="button"
              className="btn btn-primary"
              role="button"
              target="_blank"
              rel="noreferrer"
              href={`${SERVER_ASSETS}/${data.evidence.id}`}
              download
            >
              Ver Evidencia
            </a>
          )}
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Proof;
