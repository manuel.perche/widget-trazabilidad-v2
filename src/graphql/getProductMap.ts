import { gql } from "@apollo/client";

export const GET_PRODUCT_MAP = gql`
  query product_by_id($id: ID!) {
    product_by_id(id: $id) {
      map {
        lat
        lng
        zoom
        points {
          id
          nombre
          descripcion
          location {
            lat
            lng
          }
        }
      }
    }
  }
`;
