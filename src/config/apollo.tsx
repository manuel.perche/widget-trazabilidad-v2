import { ApolloClient, InMemoryCache } from "@apollo/client";
import { HOST_SERVER_API } from "../utils/constants";

const createApolloClient = () => {
  return new ApolloClient({
    uri: `${HOST_SERVER_API}/graphql`,
    cache: new InMemoryCache(),
  });
};

export default createApolloClient;
