import Mapa, { IMapProps } from "./Map/Map";
import createApolloClient from "./config/apollo";
import { ApolloProvider } from "@apollo/client";
import "bootstrap/scss/bootstrap.scss";
import "bootstrap/dist/js/bootstrap.min.js";
import "./App.css";

function MapApp(props: IMapProps) {
  const client = createApolloClient();

  return (
    <ApolloProvider client={client}>
        <Mapa {...props} />
    </ApolloProvider>
  );
}

export default MapApp;
