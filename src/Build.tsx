// import React from 'react'
// import ReactDOM from 'react-dom/client'
// import App from './App.tsx'
// import TraceWebComponent from "./web-component.tsx";

import r2wc from "@r2wc/react-to-web-component";
import TraceApp from "./TraceApp";
import MapApp from "./MapApp";

const WebTrace = r2wc(TraceApp, {
  props: {
    productId: "string",
    qrId: "string",
  },
});

const WebMap = r2wc(MapApp, {
  props: {
    productId: "string",
  },
});

// ReactDOM.createRoot(document.getElementById('root')!).render(
//   <React.StrictMode>
//     <App />
//   </React.StrictMode>,
// )

customElements.define("ucropit-trace", WebTrace);
customElements.define("ucropit-map", WebMap);
