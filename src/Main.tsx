import React from "react";
import ReactDOM from "react-dom/client";
// import TraceApp from "./TraceApp.tsx";
import MapApp from "./MapApp.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    {/* <TraceApp productId="9b3b048c-03b6-42ce-a48a-9789eb4c91af" qrId="a038a37c-1d52-4c01-870a-b4fe2ea86729" /> */}
    {/* <TraceApp productId="9b3b048c-03b6-42ce-a48a-9789eb4c91af" /> */}
    <div className="main-wrapper">
      <MapApp productId="9b3b048c-03b6-42ce-a48a-9789eb4c91af" />
    </div>
  </React.StrictMode>
);
