import { ITraceProps, Trace } from "./Trace/Trace";
import createApolloClient from "./config/apollo";
import { ApolloProvider } from "@apollo/client";
import "bootstrap/scss/bootstrap.scss";
import 'bootstrap/dist/js/bootstrap.min.js'
import './App.css'

function TraceApp(props: ITraceProps) {
  const client = createApolloClient();

  return (
    <ApolloProvider client={client}>
      <Trace {...props} />
    </ApolloProvider>
  );
}

export default TraceApp;
